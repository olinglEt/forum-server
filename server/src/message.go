package main

// Message 掲示板のメッセージを表す構造体
type Message struct {
	ID                     string
	Content                string
	CreatedBy              string
	CreatedByAsDisplayName string
	CreatedAt              string
}
