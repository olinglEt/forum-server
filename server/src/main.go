package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var dao *DbAccessor

func main() {
	router := gin.Default()
	router.Use(cors.Default())

	dao = new(DbAccessor)
	dao.Connect()
	dao.CreateTable()

	defer dao.Close()
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	router.GET("/", getThreadList)
	router.GET("/thread/show/:id", getThread)

	/* 認証が必要なメソッド */
	router.Use(AuthMiddleware())
	router.POST("/thread/add", createThread)
	router.POST("/message/add", addMessage)
	router.POST("/remove", removeAll)

	router.Run(":5001")
}
