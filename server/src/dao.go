package main

import (
	"database/sql"
	"errors"
	"time"

	_ "github.com/lib/pq"
)

// DbAccessor Database Accessor Objectを定義する構造体
type DbAccessor struct {
	client *sql.DB
}

// Connect DBに接続する
func (dao *DbAccessor) Connect() {
	db, err := sql.Open("postgres", "host=db_api port=5432 user=postgres password=secret sslmode=disable")

	if err != nil {
		time.Sleep(time.Second * 10)
		dao.Connect()
	}

	err = db.Ping()
	if err != nil {
		time.Sleep(time.Second * 10)
		dao.Connect()
	}
	dao.client = db
}

// ReconnectIfNotConnected  DBの接続がなければ接続し直す
func (dao *DbAccessor) ReconnectIfNotConnected() {
	if dao.client == nil {
		dao.Connect()
	}
}

// CreateTable テーブルが存在しなければ作成する
func (dao *DbAccessor) CreateTable() {
	dao.ReconnectIfNotConnected()
	dao.client.Query(`create table if not exists user_detail (
		username text primary key,
		display_name text
		)`)
	dao.client.Query(`create table if not exists threads (
		thread_id serial primary key,
		title  text not null,
		created_by text,
		created_at text,
		foreign key (created_by) references user_detail(username)
		)`)
	dao.client.Query(`create table if not exists messages (
		message_id serial primary key,
		thread_id serial,
		content text not null,
		created_by text,
		created_at text,
		foreign key (thread_id) references threads(thread_id),
		foreign key (created_by) references user_detail(username)
		)`)
}

// GetThreadList 全てのスレッドを取得する
func (dao *DbAccessor) GetThreadList() ([]Thread, error) {
	rows, err := dao.client.Query(`select t.thread_id, t.title, t.created_by, t.created_at, count(m.thread_id) from threads t
		left outer join messages m on t.thread_id = m.thread_id 
		group by t.thread_id
		order by t.thread_id desc`)
	if err != nil {
		return []Thread{}, errors.New("sql error")
	}

	var resList = []Thread{}
	defer rows.Close()
	for rows.Next() {
		thread := Thread{}
		err = rows.Scan(&thread.ID, &thread.Title, &thread.CreatedBy, &thread.CreatedAt, &thread.MessageCount)
		if err != nil {
			return nil, errors.New("sql error")
		}
		resList = append(resList, thread)
	}
	return resList, nil
}

// GetThread IDをもとにスレッド内のメッセージを全て取得する
func (dao *DbAccessor) GetThread(id string) []Message {
	rows, err := dao.client.Query(`select message_id, content, created_by, ud.display_name,created_at from messages
		join user_detail ud on created_by = ud.username
		where thread_id = $1
		order by message_id asc`, id)
	var resList = []Message{}
	defer rows.Close()
	for rows.Next() {
		message := Message{}
		err = rows.Scan(&message.ID, &message.Content, &message.CreatedBy, &message.CreatedByAsDisplayName, &message.CreatedAt)
		if err != nil {
			return resList
		}
		resList = append(resList, message)
	}
	return resList
}

// CreateThread スレッドを新規作成する
func (dao *DbAccessor) CreateThread(title string, username string, displayName string) {
	row := dao.client.QueryRow(`select count(*) from user_detail where username = $1`, username)
	var num int
	row.Scan(&num)
	if num == 0 {
		dao.client.Exec(`insert into user_detail (username, display_name)
			values ($1, $2)`, username, displayName)
	}
	timeString := getTimeString()
	dao.client.Exec(`insert into threads (title, created_by, created_at)
		values ($1, $2, $3)`, title, username, timeString)
}

// AddMessage 投稿を追加する
func (dao *DbAccessor) AddMessage(id string, content string, username string, displayName string) []Message {
	row := dao.client.QueryRow(`select count(*) from user_detail where username = $1`, username)
	var num int
	row.Scan(&num)
	if num == 0 {
		dao.client.Exec(`insert into user_detail (username, display_name)
		values ($1, $2)`, username, displayName)
	}
	timeString := getTimeString()
	dao.client.Exec(`insert into messages (thread_id, content, created_by, created_at)
		values ($1, $2, $3, $4)`, id, content, username, timeString)
	return dao.GetThread(id)
}

// RemoveMessagesByUsername ユーザー名を指定して投稿を全削除する
func (dao *DbAccessor) RemoveMessagesByUsername(username string) error {
	_, err := dao.client.Exec(`delete from messages where created_by = $1`, username)
	return err
}

// RemoveThreadsByUsername ユーザ名を指定してスレッドを全削除する
func (dao *DbAccessor) RemoveThreadsByUsername(username string) error {
	_, err := dao.client.Exec(`delete from messages where thread_id in (
		select t.thread_id from threads t where t.created_by = $1)`, username)
	if err != nil {
		return err
	}

	_, err = dao.client.Exec(`delete from threads where created_by = $1`, username)
	return err
}

// Close 接続を閉じる
func (dao *DbAccessor) Close() {
	if dao.client != nil {
		dao.client.Close()
	}
}

func getTimeString() string {
	location := time.FixedZone("Asia/Tokyo", 9*60*60)
	return time.Now().In(location).Format("2006/1/2 15:04:05")
}
