package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/gin-gonic/gin"
)

// UserInfo ユーザーの情報を表す構造体
type UserInfo struct {
	Username    string
	DisplayName string
}

// AuthMiddleware 認証ミドルウェア
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.PostForm("token")
		val := url.Values{}
		val.Add("token", token)
		host := os.Getenv("JWT_AUTH_SERVER_HOST")
		res, err := http.PostForm(host+"/validate", val)

		if err != nil {
			fmt.Println(err.Error())
			c.Set("Authorized", false)

		} else {
			if res.StatusCode != 200 {
				c.Set("Authorized", false)
			} else {
				var user UserInfo
				res, _ := ioutil.ReadAll(res.Body)
				json.Unmarshal(res, &user)
				c.Set("Authorized", true)
				c.Set("Username", user.Username)
				c.Set("DisplayName", user.DisplayName)
			}
		}
	}
}
