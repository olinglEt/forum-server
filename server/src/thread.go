package main

// Thread 掲示板のスレッドを表す構造体
type Thread struct {
	ID           string
	Title        string
	CreatedBy    string
	CreatedAt    string
	MessageCount int
}
