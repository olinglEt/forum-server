package main

import (
	"github.com/gin-gonic/gin"
)

func getThreadList(c *gin.Context) {
	dao.ReconnectIfNotConnected()
	list, _ := dao.GetThreadList()
	c.JSON(200, gin.H{
		"threads": list,
	})
}

func getThread(c *gin.Context) {
	dao.ReconnectIfNotConnected()
	id := c.Param("id")
	list := dao.GetThread(id)
	c.JSON(200, gin.H{
		"messages": list,
	})
}

func createThread(c *gin.Context) {
	isAuthorized, _ := c.Get("Authorized")

	if !isAuthorized.(bool) {
		c.JSON(500, gin.H{
			"err": "認証に失敗しました",
		})
		return
	}

	title := c.PostForm("title")
	username, _ := c.Get("Username")
	displayName, _ := c.Get("DisplayName")
	dao.CreateThread(title, username.(string), displayName.(string))
	list, _ := dao.GetThreadList()
	c.JSON(200, gin.H{
		"messages": list,
	})
}

func addMessage(c *gin.Context) {
	isAuthorized, _ := c.Get("Authorized")

	if !isAuthorized.(bool) {
		c.JSON(500, gin.H{
			"err": "認証に失敗しました",
		})
		return
	}
	id := c.PostForm("id")
	content := c.PostForm("content")
	username, _ := c.Get("Username")
	displayName, _ := c.Get("DisplayName")
	list := dao.AddMessage(id, content, username.(string), displayName.(string))
	c.JSON(200, gin.H{
		"messages": list,
	})

}

func removeAll(c *gin.Context) {
	isAuthorized, _ := c.Get("Authorized")

	if !isAuthorized.(bool) {
		c.JSON(500, gin.H{
			"err": "認証に失敗しました",
		})
		return
	}
	username, _ := c.Get("Username")
	err := dao.RemoveMessagesByUsername(username.(string))
	if err != nil {
		c.JSON(500, gin.H{
			"err": "削除に失敗しました",
		})
		return
	}
	err = dao.RemoveThreadsByUsername(username.(string))
	if err != nil {
		c.JSON(500, gin.H{
			"err": "削除に失敗しました",
		})
	} else {
		c.JSON(200, gin.H{
			"success": true,
		})
	}

}
